﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovement : MonoBehaviour
{
    public float moveSpeed;
    private Rigidbody2D myRigidbody;
    private SpriteRenderer mySpriteRenderer;
    private Animator myAnimator;
    public bool isWalking;
    public float walkTime;
    public float walkCounter;
    public float waitTime;
    private float waitCounter;
    private int walkDirection;
    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        myAnimator = GetComponent<Animator>();
        waitCounter = waitTime;
        walkCounter = walkTime;
        ChooseDirection();
    }

    // Update is called once per frame
    void Update()
    {
        if (isWalking)
        {
            walkCounter -= Time.deltaTime;
            switch (walkDirection)
            {
                
                case 0:
                   myRigidbody.velocity = new Vector2(moveSpeed, 0f);
                    mySpriteRenderer.flipX = false;
                    myAnimator.SetBool("Walk", true);
                    myAnimator.SetBool("Idle", false);
                    break;
                case 1:
                    myRigidbody.velocity = new Vector2(-moveSpeed, 0f);
                    mySpriteRenderer.flipX = true;
                    myAnimator.SetBool("Walk", true);
                    myAnimator.SetBool("Idle", false);
                    break;

            }
            if(walkCounter < 0)
            {
                isWalking = false;
                myAnimator.SetBool("Idle", true);
                myAnimator.SetBool("Walk", false);
                waitCounter = waitTime;
            }
        }
        else
        {
            waitCounter -= Time.deltaTime;
            myRigidbody.velocity = Vector2.zero;
            if (waitCounter < 0)
            {
                ChooseDirection();
            }
        }
    }
    public void ChooseDirection()
    {
        walkDirection = Random.Range(0,2);
        isWalking = true;
        walkCounter = walkTime;
    }
}
