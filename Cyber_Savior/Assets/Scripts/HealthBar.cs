﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private HealthSystem healthsystem;
    [SerializeField] GameObject bar;

    public void Setup (HealthSystem hs)
    {
        this.healthsystem = hs;
    }

    private void Update()
    {
        bar.transform.localScale = new Vector3(healthsystem.getHealthPercentage(), 0.5f);
    }
}
