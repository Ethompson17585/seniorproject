﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public ParticleSystem ShootPS;
    public ParticleSystem SuperJumpPS;
    public int killCountL1 = 0;
    public int killCountL2 = 0;
    public GameObject DaveNPC;
    public bool DaveTravel = false;
    public bool DaveLand = false;
    public HealthSystem healthSystem;
    public HealthBar healthBar;
    [SerializeField] float runSpeed = 5f;
    [SerializeField] float jumpSpeed = 0f;
    [SerializeField] float climbSpeed = 5f;
    [SerializeField] Vector2 deathKick = new Vector2(25f, 25f);
    [SerializeField] GameObject AttackHitBox;
    [SerializeField] Object bulletRef;
    [SerializeField] public GameObject Teleporter1;
    [SerializeField] GameObject HintText;
    [SerializeField] ParticleSystem ps;
    [SerializeField] public int playersHealth = 3;
    [SerializeField] int damageTaken = 1;
    [SerializeField] float timeBtwDamage = 2f;
    public static int sceneNumber;
    public static bool loaded;
    public static bool allowedToShoot = true;
    public bool paused;
    public bool knockBack;
    float shootTimer = 0f;
    float shootCoolDown = 2f;
    float maskWait = 1.5f;
    bool isAlive = true;
    public bool canMove = true;
    bool maskActive = false;
    bool isAttacking;
    bool isShooting = false;
    bool ismelee = false;
    bool canSuperJump = false;
    public bool damageToPlayer = true;
    private CheckpointMaster cm;
    Animator myAnimator;
    Rigidbody2D myRigidBody;
    CapsuleCollider2D myBodyCollider;
    BoxCollider2D myFeet;
    Transform myPos;
    [SerializeField] GameObject mask;
    //Rigidbody2D originalConstraints;
    float gravityScaleAtStart;
    // Start is called before the first frame update
    void Start()
    {
        healthSystem = new HealthSystem(100);
        healthBar.Setup(healthSystem);
        playersHealth = healthSystem.getHealth();
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myBodyCollider = GetComponent<CapsuleCollider2D>();
        myFeet = GetComponent<BoxCollider2D>();
        myPos = GetComponent<Transform>();
        cm = GameObject.FindGameObjectWithTag("CM").GetComponent<CheckpointMaster>();
        myPos.position = new Vector2(cm.lastCheckPointposX, cm.lastCheckPointposY);
        AttackHitBox.SetActive(false);
        bulletRef = Resources.Load("Character_Bullet");
        //originalConstraints = myRigidBody.constraints;
        gravityScaleAtStart = myRigidBody.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {
       // Debug.Log("Player y Velocity: " + myRigidBody.velocity.y.ToString());
        if (!isAlive) { return; }
        Movement();
        Attack();
        Jump();
        EnableMask();
        FlipSprite();
        ClimbLadder();
        Die();
        cantLeave();
<<<<<<< HEAD
        ChangeTimeScale();
        sceneNumber = SceneManager.GetActiveScene().buildIndex;
    }
    public void ChangeTimeScale()
    {
        if (paused)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }
=======
>>>>>>> 97b1c3fb7f0865fe0890b8f0fc9e88044bcaf509
    }
    //press q then start to count down from 5 
    //if q is pressed and feet are touching the ground, pull up the players mask, reserve the bool each press and play mask animation
    private void EnableMask()
    {
        if (canMove)
        {
            if (Input.GetKeyDown(KeyCode.Q) && myFeet.IsTouchingLayers(LayerMask.GetMask("Foreground")) && mask.activeSelf)
            {
                if (maskActive == true) //if mask is already on
                {
                    myAnimator.Play("PlayerMaskDown");
                    StartCoroutine(DelayMask());
                    maskActive = false;
                }
                else //if mask is off
                {
                    myAnimator.Play("PlayerMaskUp");
                    StartCoroutine(DelayMask());
                    maskActive = true;
                }
            }
        }
    }

    private IEnumerator DelayMask()
    {
        canMove = false;
        myRigidBody.velocity = new Vector2(0f, 0f);
        yield return new WaitForSeconds(maskWait);
        canMove = true;
    }

    //if no keys are pressed play idle animation
    //if keys a and d are pressed add x velocity to move the player and play walk animation 
    //if shift is held, the player runs and plays a run animation, player can only run with mask up
    private void Movement()
    {
        if (canMove)
        {
            bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
            float controlThrow = CrossPlatformInputManager.GetAxis("Horizontal");
            Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, myRigidBody.velocity.y);
            myRigidBody.velocity = playerVelocity;

            if (maskActive == true) //mask on
            {
                if (isAttacking == false && myRigidBody.velocity.x == 0 && myFeet.IsTouchingLayers(LayerMask.GetMask("Foreground"))) //not moving and on ground
                {
                    myAnimator.SetBool("WalkMask", false);
                    myAnimator.SetBool("IdleMask", true);
                    myAnimator.SetBool("Running", false);
                    myAnimator.SetBool("Falling", false);
                    myAnimator.SetBool("Mask", false);
                    myAnimator.SetBool("Idle", false);
                }
                if (myRigidBody.velocity.x != 0 && !Input.GetKey(KeyCode.LeftShift) && isAttacking == false && myFeet.IsTouchingLayers(LayerMask.GetMask("Foreground"))) //moving on ground
                {
                    myAnimator.SetBool("IdleMask", false);
                    myAnimator.SetBool("WalkMask", true);
                    myAnimator.SetBool("Running", false);
                    myAnimator.SetBool("Falling", false);
                    myAnimator.SetBool("Mask", false);
                    myAnimator.SetBool("Idle", false);
                }
                if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Foreground")) && isAttacking == false && !myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Climbing"))) //in air not attacking
                {
                    myAnimator.SetBool("Falling", true);
                    myAnimator.Play("PlayerMaskInAirMiddle");
                }
                if (isAttacking == true) //if attacking
                {
                    myAnimator.SetBool("IdleMask", false);
                    myAnimator.SetBool("Idle", false);
                    myAnimator.SetBool("WalkMask", false);
                    myAnimator.SetBool("Running", false);
                    myAnimator.SetBool("Falling", false);
                    myAnimator.SetBool("Mask", false);
                    if (ismelee) //if melee
                    {
                        int index = UnityEngine.Random.Range(1, 3);
                        myAnimator.Play("PlayerMelee" + index);
                        myAnimator.SetBool("Melee", isAttacking);
                        StartCoroutine(DoAttack());
                    }
                    else if (isShooting) //if shooting
                    {
                        int index = UnityEngine.Random.Range(1, 3);
                        myAnimator.Play("PlayerMelee" + index);
                        myAnimator.SetBool("Melee", isAttacking);
                        StartCoroutine(DoAttack());
                    }
                }
            }
            else //mask off
            {
                if (isAttacking == false && myRigidBody.velocity.x == 0 && myFeet.IsTouchingLayers(LayerMask.GetMask("Foreground"))) //not moving and on ground
                {
                    myRigidBody.constraints &= ~RigidbodyConstraints2D.FreezePosition;
                    myAnimator.SetBool("Walk", false);
                    myAnimator.SetBool("Idle", true);
                    myAnimator.SetBool("Running", false);
                    myAnimator.SetBool("Falling", false);
                }
                if (myRigidBody.velocity.x != 0 && !Input.GetKey(KeyCode.LeftShift) && isAttacking == false && myFeet.IsTouchingLayers(LayerMask.GetMask("Foreground"))) //moving on ground
                {
                    myAnimator.SetBool("Idle", false);
                    myAnimator.SetBool("Walk", true);
                    myAnimator.SetBool("Running", false);
                    myAnimator.SetBool("Falling", false);
                }
            }
            if (myFeet.IsTouchingLayers(LayerMask.GetMask("Foreground"))  && Input.GetKey(KeyCode.LeftShift) && maskActive == true && myRigidBody.velocity.x != 0)
            {
                runSpeed = 4.5f;
                canSuperJump = true;
                myAnimator.SetBool("Walk", false);
                myAnimator.SetBool("Running", true);
                myAnimator.SetBool("Falling", false);
            }
            else
            {
                runSpeed = 3.5f;
                canSuperJump = false;
                myAnimator.SetBool("Running", false);
            }

        }
        else
        {
            myRigidBody.velocity = new Vector2(0f, 0f);
            myAnimator.SetBool("IdleMask", false);
            myAnimator.SetBool("Idle", false);
            myAnimator.SetBool("WalkMask", false);
            myAnimator.SetBool("Walk", false);
            myAnimator.SetBool("Running", false);
            myAnimator.SetBool("Falling", false);
            myAnimator.SetBool("Mask", false);
            return;
        }
    }

    public void Listening()
    {
        if (DaveNPC.GetComponent<DaveTheNPCScript>().Interactable && maskActive)
            {
            Debug.Log("Yes");
            myAnimator.SetBool("IdleMask", true);
            myAnimator.SetBool("Idle", false);
            myAnimator.SetBool("WalkMask", false);
            myAnimator.SetBool("Walk", false);
            myAnimator.SetBool("Running", false);
            myAnimator.SetBool("Falling", false);
            myAnimator.SetBool("Mask", false);
        }
            else if (DaveNPC.GetComponent<DaveTheNPCScript>().Interactable && !maskActive)
            {
            Debug.Log("No");
            myAnimator.SetBool("IdleMask", false);
            myAnimator.SetBool("Idle", true);
            myAnimator.SetBool("WalkMask", false);
            myAnimator.SetBool("Walk", false);
            myAnimator.SetBool("Running", false);
            myAnimator.SetBool("Falling", false);
            myAnimator.SetBool("Mask", false);
        }
    }
    private void cantLeave()
    {
        if(SceneManager.GetActiveScene().name == "Level_1")
        {
            if(killCountL1 < 6 && transform.position.x >= 44)
            {
                transform.position = new Vector2(44f, transform.position.y);
            }
        }
        if (SceneManager.GetActiveScene().name == "Level_2")
        {
            if (killCountL2 < 11 && transform.position.x >= 110)
            {
                transform.position = new Vector2(110f, transform.position.y);
            }
        }
    }
    private void ClimbLadder()
    {
        if (canMove && maskActive)
        {
            bool playerHasVerticalSpeed = Mathf.Abs(myRigidBody.velocity.y) > Mathf.Epsilon;
            if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Climbing")) && playerHasVerticalSpeed == false)
            {
                myAnimator.SetBool("StillClimb", true);
                myAnimator.SetBool("Climb", false);
            }
            if (!myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Climbing")))
            {
                if (maskActive)
                {
                    myAnimator.SetBool("Climb", false);
                    myRigidBody.gravityScale = gravityScaleAtStart;
                    return;
                }
                else
                {
                    myAnimator.SetBool("Climb", false);
                    myAnimator.SetBool("Idle", true);
                    myRigidBody.gravityScale = gravityScaleAtStart;
                    return;
                }
            }
            if (!CrossPlatformInputManager.GetButtonDown("Jump"))
            {
                float controlThrow = CrossPlatformInputManager.GetAxis("Vertical");
                Vector2 climbVelocity = new Vector2(myRigidBody.velocity.x, controlThrow * climbSpeed);
                myRigidBody.velocity = climbVelocity;
                myRigidBody.gravityScale = 0f;
                myAnimator.SetBool("StillClimb", false);
                myAnimator.SetBool("Climb", playerHasVerticalSpeed);
            }
        }
    }

    //if space is pressed, add velocity to the player | if Shift and space are pressed, add more velocity to the player
    private void Jump()
    {
        if (canMove)
        {
            if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Foreground")) && !myFeet.IsTouchingLayers(LayerMask.GetMask("Climbing")) || maskActive == false)
            {
                jumpSpeed = 0f;
                return;
            }
            else if (canSuperJump && CrossPlatformInputManager.GetButton("Jump") && maskActive == true)
            {
                jumpSpeed = 9f;
                myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, jumpSpeed);
                SuperJumpPS.Play();
                jumpTimer();
                
            }
            else if (CrossPlatformInputManager.GetButtonDown("Jump") && maskActive == true)
            {
                jumpSpeed = 8f;
                myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, jumpSpeed);
                jumpTimer();
            }
            jumpSpeed = 0f;
        }
    }

    //Controls the players attacks
    public void Attack()
    {
        shootTimer += Time.deltaTime;
        if (Input.GetButtonDown("Fire1") && !isAttacking && maskActive) //if left click gets pressed, attack and pick 1 of the 2 animations at random
        { 
            isAttacking = true;
            ismelee = true;
        }
        else if (Input.GetButtonDown("Fire2") && !isAttacking && maskActive) //players ability to shoot, flip bullet left or right and negative or positve velocity depending on players direction
        {
            isAttacking = true;
            isShooting = true;
            ShootPS.Play();

            //prevent player from spam shooting
            if (allowedToShoot)
            {
                GameObject bullet = (GameObject)Instantiate(bulletRef);
                allowedToShoot = false;
                if (shootTimer < shootCoolDown)
                {
                    bullet.GetComponent<PlayerBullet>().destroyNow();
                }
                if (transform.localScale.x < 0)
                {
                    bullet.transform.localScale = new Vector3(-1, 1, 1);
                    bullet.transform.position = new Vector3(transform.position.x + -.4f, transform.position.y, -1);
                    bullet.GetComponent<PlayerBullet>().bulletVelocity = -3;
                    //bullet.GetComponent<PlayerBullet>().destroySelf();
                }
                else if (transform.localScale.x > 0)
                {
                    bullet.transform.localScale = new Vector3(1, 1, 1);
                    bullet.transform.position = new Vector3(transform.position.x + .4f, transform.position.y, -1);
                    bullet.GetComponent<PlayerBullet>().bulletVelocity = 3;
                    //bullet.GetComponent<PlayerBullet>().destroySelf();
                }
            }
            else
            {
                return;
            }
        }
        else
        {
            isShooting = false;
            ismelee = false;
            isAttacking = false;
        }
    }

    //performs a attack, also activates hit box for a set amount of time and returns the player back to idle after melee
    IEnumerator DoAttack()
    {
        AttackHitBox.SetActive(true);
        yield return new WaitForSeconds(.4f);
        AttackHitBox.SetActive(false);
        isAttacking = false;
        myAnimator.SetBool("Melee", isAttacking);

    }

    //Controls what happens when the player runs out of health
    public void Die()
    {
        playersHealth = healthSystem.getHealth();
        //if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Enemy", "Hazards", "Bullet")))
        //{
        //healthSystem.Damage(10);
        //StartCoroutine(damageTimer());
        if (playersHealth <= 0)
            {
                isAlive = false;
                myAnimator.SetTrigger("Die");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                GetComponent<Rigidbody2D>().velocity = deathKick;
            }
        //}

        if (myRigidBody.velocity.y <= -16)
        {
            transform.position = new Vector2(-2, -3.4f);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            myRigidBody.velocity = Vector2.zero;
        }
    }

    //flips player sprite if moving
    private void FlipSprite()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        if (playerHasHorizontalSpeed)
        {
            transform.localScale = new Vector2(Mathf.Sign(myRigidBody.velocity.x), 1f);
        }
    }

    public void SavePlayer()
    {
        SaveSystem.SavePlayer(this);
        Debug.Log("Player Loaded");
    }

    public void LoadPlayer()
    {
        PlayerData data = SaveSystem.LoadPlayer();
        sceneNumber = data.level;
        Debug.Log(sceneNumber);
        playersHealth = data.health;
        SceneManager.LoadScene(sceneNumber);
        Vector2 position;
        position.x = data.postition[0];
        position.y = data.postition[1];
        transform.position = position;
        Debug.Log(transform.position);
        Debug.Log("Player Loaded");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            //Debug.Log(playersHealth);
            healthSystem.Damage(10);
            playersHealth = healthSystem.getHealth();
            //Debug.Log(playersHealth);
        }
        if (collision.gameObject.tag == "Enemy" && ismelee)
        {
            collision.gameObject.GetComponent<enemyLogic>().healthSystem.Damage(10);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Travel")
        {
            Destroy(collision.gameObject);
            DaveTravel = true;
        }
        else
        {
            DaveTravel = false;
        }
        if (collision.gameObject.tag == "Land")
        {
            Destroy(collision.gameObject);
            DaveLand = true;
        }
        else
        {
            DaveLand = false;
        }
        if (collision.gameObject.tag == "HealthPack")
        {
            Destroy(collision.gameObject);
            healthSystem.Heal(50);
        }
    }


    private IEnumerator jumpTimer()
    {
        yield return new WaitForSeconds(0.3f);
    }
}
