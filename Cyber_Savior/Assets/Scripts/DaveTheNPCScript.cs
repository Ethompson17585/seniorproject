﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DaveTheNPCScript : MonoBehaviour
{
    Rigidbody2D myRigidBody;
    CapsuleCollider2D myBodyCollider;
    SpriteRenderer mySpriteRenderer;
    Animator myAnimator;
    public Player Player;
    [SerializeField] GameObject Dialog;
    [SerializeField] GameObject Background;

    int index = 0;
    int index1 = 0;
    int L2index = 0;
    int L2index1 = 0;
    int i = 0;
    int i2 = 16;
    public bool doneTalking = false;

    [SerializeField] GameObject mask;
    [SerializeField] GameObject chip;

    // [SerializeField] Vector2 LandingPossition;
    public bool isTalking = true;
    float flyspeed = 7f;
    Vector2 target;
    Vector2 position;
    float step;
    public bool takeoff = false;
    public bool travel = false;
    public bool land = false;
    public bool Interactable = true;
    public bool talkToPlayer = false;
    List<string> StoryText = new List<string>();
    List<Vector2> LandingLoctions = new List<Vector2>();
    List<Vector2> LandingLoctionsL2 = new List<Vector2>();
    List<Vector2> TravelLocations = new List<Vector2>();
    List<Vector2> TravelLocationsL2 = new List<Vector2>();
    List<Vector2> PlayerPoints = new List<Vector2>();
    Vector2 x;
    Vector2 y;

    // Start is called before the first frame update
    void Start()
    {
        if(SceneManager.GetActiveScene().name == "Level_1")
        {
            mask.SetActive(false);
            chip.SetActive(false);
        }
        else if(SceneManager.GetActiveScene().name == "Level_2")
        {
            mask.SetActive(true);
            chip.SetActive(true);
        }
        
        myRigidBody = GetComponent<Rigidbody2D>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        myAnimator = GetComponent<Animator>();
        myBodyCollider = GetComponent<CapsuleCollider2D>();
        target = new Vector2(0f, 0f);
        position = gameObject.transform.position;
        //Level1
        LandingLoctions.Add(new Vector2(13.46f, -59.4804f));
        LandingLoctions.Add(new Vector2(20.48f, -52.42f));
        LandingLoctions.Add(new Vector2(46.49f, -59.4804f));
        LandingLoctions.Add(new Vector2(64.5f, -59.4804f));
        TravelLocations.Add(new Vector2(13.46f, -35f));
        TravelLocations.Add(new Vector2(20.48f, -35f));
        TravelLocations.Add(new Vector2(46.49f, -35f));
        TravelLocations.Add(new Vector2(64.5f, -35f));
        PlayerPoints.Add(new Vector2(15.47f, -54.4804f));
        PlayerPoints.Add(new Vector2(39.52f, -59.4804f));
        PlayerPoints.Add(new Vector2(47.2f, -59.4804f));
        //Level2
        LandingLoctionsL2.Add(new Vector2(90f, -59.4804f));
        LandingLoctionsL2.Add(new Vector2(90f, -59.4804f));
        LandingLoctionsL2.Add(new Vector2(123.7f, -43.36f));
        LandingLoctionsL2.Add(new Vector2(145f, -49.37f));
        LandingLoctionsL2.Add(new Vector2(205.47f, -59.4804f));
        TravelLocationsL2.Add(new Vector2(90f, -35f));
        TravelLocationsL2.Add(new Vector2(123.7f, -35));
        TravelLocationsL2.Add(new Vector2(145f, -35f));
        TravelLocationsL2.Add(new Vector2(205.47f, -35f));
        //Story
        StoryText.Add("So your Mason, I thought you'd be taller. Allow me to introduce myself. I am one of the leaders of the Allegiance, " +
            "you can call me Dave. I am here to inform you that you have sucessfuly accomplished your training and are now a member of the " +
            "Allegiance...");
        StoryText.Add("You happened to join just in the nick of time. Tonight we are going to purge Cyber City of Daxton Shade and all of" +
            "his minions. I will help guide you along the way...");
        StoryText.Add("Here, take these...");
        StoryText.Add("This mask will allow you to hide your identity and use all of your abilities, however, when you wear it, enemies will attack on sight." +
            "Taking your mask off will allow you to blend in with the local citizens and sneak past guards. Remember that you can press Q to raise and lower your mask");
        StoryText.Add("That chip I gave you will allow you to hack into any terminal in the city and use its network to transport yourself to another linked terminal." +
            "You can do this by standing next to any terminal and pressing E.");
        StoryText.Add("That chip also gives you the ability to generate your own fuzzion blasts to deal massive amounts of dammage to enemies. Be aware that the chip can" +
            " only support one blast at a time. You can activate your fuzzion blast by pressing the right mouse button.");
        StoryText.Add("Now that you have what you need, its time to take back our city. Lets start by getting you warmed up. Remember that special abilities only work while " +
            "your mask is up. Lets see what youve got, meet me on top of those billboards up there.");
        StoryText.Add("");
        StoryText.Add("Great job, I can see why the Allegiance was so interested in you!");
        StoryText.Add("We will start our perge of Daxton Shade here in the Green district first. This building is full of Shade minions that have ruined the local civilians way of life." +
            "It is your job to get rid of them. Do not leave the building until they are all gone.There are healthpacks scattard arround the world that you can pickup to replenish some of your health");
        StoryText.Add("Good Luck, Ill see you on the other side.");
        StoryText.Add("");
        StoryText.Add("Nice job taking care of those minions! All thats left to take care of here in the Green District is Daxtons Robotic Minion that he uses to maintain control. Go take him out!");
        StoryText.Add("");
        StoryText.Add("Great Job, now lets go liberate the Purple District. Ill Meet you there.");
        StoryText.Add("");
        StoryText.Add("Welcome to the Purple District, lets clear out all of Shade's Minions");
        StoryText.Add("");
        StoryText.Add("Great Job! The Purple District is now clear!");
        StoryText.Add("All that is left before Shade is the Blue District. Lets go set those citizens free.");
        StoryText.Add("");
        StoryText.Add("Welcome to the Blue District, Shade has heard we are comming so he withdrew most of his forces back to his bad. However, " +
            "a few of his minions still remain in the district, lets go eliminate them. Ill meet you outside Shade Tower.");
        StoryText.Add("");
        StoryText.Add("This is it, Shade Tower. Everything has come down to this.");
        StoryText.Add("The security will be tight so use everything you have learned so far to take down Daxton Shade and free the city.");
        StoryText.Add("");
    }

    // Update is called once per frame
    void Update()
    {
        step = flyspeed * Time.deltaTime;
        FlipSprite();
        CheckInput();
        Talk();
        isPlayerClose();
        //Story();

        if (doneTalking && !travel)
        {
            land = false;
            travel = false;
            Takeoff();
        }
        if (travel)
        {
            doneTalking = false;
            land = false;
            Travel();
        }
        if (land)
        {
            travel = false;
            doneTalking = false;
            Land();
        }
    }
    private void FlipSprite()
    {
        bool playerIsLeft = Player.transform.position.x < myRigidBody.transform.position.x;
        if (playerIsLeft)
        {
            mySpriteRenderer.flipX = true;
        } else
        {
            mySpriteRenderer.flipX = false;
        }
    }

    private void CheckInput()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            doneTalking = true;
        }
        else if (Player.DaveTravel && travel == false)
        {
            Player.DaveTravel = false;
            travel = true;
            if (SceneManager.GetActiveScene().name == "Level_1")
            {
                index1 += 1;
            }
            if (SceneManager.GetActiveScene().name == "Level_2")
            {
                L2index1 += 1;
            }

        }
        else if (Player.DaveLand && land == false)
        {
            Player.DaveLand = false;
            land = true;
            if (SceneManager.GetActiveScene().name == "Level_1")
            {
                index += 1;
            }
            if (SceneManager.GetActiveScene().name == "Level_2")
            {
                L2index += 1;
            }
        }
    }

    private void Takeoff()
    {
        target = new Vector2(transform.position.x, -35f);
        if (transform.position.y < -35 && !travel)
        {
            myAnimator.SetBool("Idle", false);
            myAnimator.SetBool("Land", false);
            myAnimator.SetBool("Takeoff", true);
            delay();
            myAnimator.SetBool("Flying", true);
            transform.position = Vector2.MoveTowards(transform.position, target, step);
            Interactable = false;
        }
        else
        {
            myRigidBody.velocity = new Vector2(0f, 0f);
            doneTalking = false;
            Interactable = true;
        }
    }

    private void Land()
    {
        if (SceneManager.GetActiveScene().name == "Level_1")
        {
            if (transform.position.y != LandingLoctions[index].y)
            {
                myAnimator.SetBool("Flying", true);
                transform.position = Vector2.MoveTowards(transform.position, LandingLoctions[index], step);
                Interactable = false;
            }
            else
            {
                myAnimator.SetBool("Idle", false);
                myAnimator.SetBool("Flying", false);
                myAnimator.SetBool("Land", true);
                myAnimator.SetBool("Takeoff", false);
                myRigidBody.velocity = new Vector2(0f, 0f);
                land = false;
                Interactable = true;
            }
        }
        if (SceneManager.GetActiveScene().name == "Level_2")
        {
            if (transform.position.y != LandingLoctionsL2[L2index].y)
            {
                transform.position = Vector2.MoveTowards(transform.position, LandingLoctionsL2[L2index], step);
            }
            else
            {
                myAnimator.SetBool("Idle", false);
                myAnimator.SetBool("Flying", false);
                myAnimator.SetBool("Land", true);
                myAnimator.SetBool("Takeoff", false);
                myRigidBody.velocity = new Vector2(0f, 0f);
                land = false;
            }
        }
    }

    private void Travel()
    {
        if (SceneManager.GetActiveScene().name == "Level_1")
        {
            myAnimator.SetBool("Idle", false);
            myAnimator.SetBool("Flying", true);
            myAnimator.SetBool("Land", false);
            myAnimator.SetBool("Takeoff", false);
            if (transform.position.y != TravelLocations[index1].y || transform.position.x != TravelLocations[index1].x)
            {
                transform.position = TravelLocations[index1];
                travel = false;
            }
            else
            {
                myRigidBody.velocity = new Vector2(0f, 0f);
                travel = false;
            }
        }
        if (SceneManager.GetActiveScene().name == "Level_2")
        {
            myAnimator.SetBool("Idle", false);
            myAnimator.SetBool("Flying", true);
            myAnimator.SetBool("Land", false);
            myAnimator.SetBool("Takeoff", false);
            if (transform.position.y != TravelLocationsL2[L2index1].y || transform.position.x != TravelLocationsL2[L2index1].x)
            {
                transform.position = TravelLocationsL2[L2index1];
            }
            else
            {
                myRigidBody.velocity = new Vector2(0f, 0f);
                travel = false;
            }
        }
    }

    private void isPlayerClose()
    {
        var diff = Player.transform.position - transform.position;
        var distance = diff.sqrMagnitude;
        if (distance <= 1.5 && !doneTalking)
        {
            Interactable = true;
            Story();
        }
        else
        {
            Interactable = false;
        }
    }

    private void Story()
    {
        if (Interactable)
        {
            myAnimator.SetBool("Idle", true);
            myAnimator.SetBool("Flying", false);
            myAnimator.SetBool("Land", false);
            myAnimator.SetBool("Takeoff", false);
            Player.canMove = false;
            Player.Listening();
            Player.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
            Dialog.SetActive(true);
            Background.SetActive(true);
            if (SceneManager.GetActiveScene().name == "Level_1")
            {
                Dialog.GetComponent<TMPro.TextMeshProUGUI>().text = StoryText[i];
                if (Input.GetKeyDown(KeyCode.Return) && Player.GetComponent<BoxCollider2D>().IsTouchingLayers(LayerMask.GetMask("Foreground")) && StoryText[i] != "")
                {
                    if (StoryText[i] == "Here, take these...")
                    {
                        mask.SetActive(true);
                        chip.SetActive(true);
                    }
                    i += 1;
                    Dialog.GetComponent<TMPro.TextMeshProUGUI>().text = StoryText[i];
                }
                else if (StoryText[i] == "")
                {
                    i += 1;
                    Interactable = false;
                    Player.canMove = true;
                    doneTalking = true;
                    return;
                }
            }
            else if (SceneManager.GetActiveScene().name == "Level_2")
            {
                Dialog.GetComponent<TMPro.TextMeshProUGUI>().text = StoryText[i2];
                if (Input.GetKeyDown(KeyCode.Return) && Player.GetComponent<BoxCollider2D>().IsTouchingLayers(LayerMask.GetMask("Foreground")) && StoryText[i2] != "")
                {
                    if (StoryText[i2] == "Here, take these...")
                    {
                        mask.SetActive(true);
                        chip.SetActive(true);
                    }
                    i2 += 1;
                    Dialog.GetComponent<TMPro.TextMeshProUGUI>().text = StoryText[i2];
                }
                else if (StoryText[i2] == "")
                {
                    i2 += 1;
                    Interactable = false;
                    Player.canMove = true;
                    doneTalking = true;
                    return;
                }
                
            }
            else
                {
                    Player.canMove = true;
                    Dialog.SetActive(false);
                    Background.SetActive(false);
                    doneTalking = false;
                    return;
                }
        } 
    }
    private IEnumerator delay()
    {
        yield return new WaitForSeconds(1f);
    }
    private void Talk()
    {
        if (Interactable)
        {

            if (talkToPlayer)
            {
                Dialog.SetActive(true);
            }
            else
            {
                Dialog.SetActive(false);
                Background.SetActive(false);
                return;
            }
        }
        else
        {
            Dialog.SetActive(false);
            Background.SetActive(false);
            return;
        }
        
        
    }
}
