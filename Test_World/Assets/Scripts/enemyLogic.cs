﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyLogic : MonoBehaviour
{
    [SerializeField] float moveSpeed = 0.5f;
    Rigidbody2D myRigidBody;
    public Transform sightBegin, sightEnd;
    [SerializeField] float radius = 3.5f;
    public GameObject Laser;
    public Transform player;
     [SerializeField]public  float timebwnShot = 2;
    public bool delayShoot = true;
    public Collider2D[] hitColliders;
    public GameObject clone;
    public bool foundPlayer = false;
    public GameObject Player;
    Animator enemyAnimator;
    public bool enemyAnim = false;
    private void Start()
    {

        enemyAnimator = GetComponent<Animator>();
        myRigidBody = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    // Update is called once per frame
    void Update()
    {
        if (IsFacingRight() )
        {
            myRigidBody.velocity = new Vector2(-moveSpeed, 0f);
        }
        else
        {
            myRigidBody.velocity = new Vector2(moveSpeed, 0f);
        }

        
        
    }

    private void FixedUpdate()
    {
        OverLapCircle();
    }

    void OverLapCircle()
    {
        hitColliders = Physics2D.OverlapCircleAll(sightBegin.position,radius);
        for(var i =0; i<hitColliders.Length;i++)
        {Debug.Log(hitColliders[i]);
            if(hitColliders[i].tag ==("Player"))
            {
                
                foundPlayer = true;
                if(foundPlayer)
                {
                    enemyAnimator.SetBool("SoldierGun",true);
                    enemyAnimator.SetBool("SoldierWalk", false);
                    moveSpeed = 0f;
                    //transform.position = Vector2.MoveTowards(transform.position, new Vector2(player.position.x, transform.position.y), moveSpeed * Time.deltaTime);
                    Shoot();
                    

                }
                
                else if(!foundPlayer)
                {
                    enemyAnimator.SetBool("SoldierGun", false);
                    enemyAnimator.SetBool("SoldierWalk", true);
                    Destroy(clone);
                    moveSpeed = 0.5f;

                }
                 
            }
        }
    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(sightBegin.position, radius);
    }

    void Shoot()
    {
        if (!IsFacingRight() && delayShoot )
        {
            
            clone = Instantiate(Laser, sightEnd.position, Quaternion.identity);
            
            clone.GetComponent<bulletScript>().speed = 0.05f;
            
            StartCoroutine(noShoot());
        }
        else if(IsFacingRight() && delayShoot)
        {
            
            clone = Instantiate(Laser, sightEnd.position, Quaternion.identity);

            clone.GetComponent<bulletScript>().speed = -0.05f;
            StartCoroutine(noShoot());
        }
    }

  
    bool IsFacingRight()
    {
        return transform.localScale.x > 0;
    }

    private IEnumerator noShoot()
    {
        delayShoot = false;
        yield return new WaitForSeconds(timebwnShot);
        delayShoot = true;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag ==("Boundary"))
        { Debug.Log("boundary");
            if (IsFacingRight())
            {
                myRigidBody.velocity = new Vector2(moveSpeed, 0f);
            }
            else
            {
                myRigidBody.velocity = new Vector2(-moveSpeed, 0f);
            }

        }
        transform.localScale = new Vector2(-(Mathf.Sign(myRigidBody.velocity.x)), 1f);
    }

   
}
