﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class teleport : MonoBehaviour
{
    [SerializeField] ParticleSystem ps;
    public GameObject partnerTeleporter;
    public GameObject Player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {

        if(collision.gameObject.tag == "Player" && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Hit Player");
            Player.transform.position = partnerTeleporter.transform.position;
            Instantiate(ps, partnerTeleporter.transform.position, partnerTeleporter.transform.rotation);
            TeleportTimer();
        }
    }

    private IEnumerator TeleportTimer()
    {
        yield return new WaitForSeconds(1f);
    }
}
