﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class enemyLogic : MonoBehaviour
{
    public ParticleSystem EnemyDie;
    public HealthSystem healthSystem;
    public HealthBar healthBar;
    public int healthAmount;
    [SerializeField] float moveSpeed = 0.5f;
    Rigidbody2D myRigidBody;
    public Transform sightEnd;
    [SerializeField] float radius = 3.5f;
    public GameObject Laser;
    [SerializeField] public float timebwnShot = 2;
    public bool delayShoot = true;
    public Collider2D[] hitColliders;
    public GameObject clone;
    public bool foundPlayer;
    public GameObject Player;
    Animator enemyAnimator;
    public bool enemyAnim = false;
    public bool isFacingRight;

    private void Start()
    {
        healthSystem = new HealthSystem(healthAmount);
        healthBar.Setup(healthSystem);
        enemyAnimator = GetComponent<Animator>();
        myRigidBody = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        Die();
        if (IsFacingRight())
        {
            myRigidBody.velocity = new Vector2(-moveSpeed, 0f);
        }
        else
        {
            myRigidBody.velocity = new Vector2(moveSpeed, 0f);
        }

    }

    private void FixedUpdate()
    {
        OverLapCircle();
    }


    //Detects the radius around the enemy and determines shooting.
    void OverLapCircle()
    {
        foundPlayer = false;
        hitColliders = Physics2D.OverlapCircleAll(transform.position, radius);
        for (var i = 0; i < hitColliders.Length; i++)
        {

            if (hitColliders[i].CompareTag("Player"))
            {
                foundPlayer = true;
                enemyAnimator.SetBool("Gun", true);
                enemyAnimator.SetBool("Walk", false);

                if (foundPlayer)
                {

                    if (Player.transform.position.x <= transform.position.x)
                    {
                        
                        Shoot();
                        transform.localScale = new Vector3(1, 1, 1);
                        enemyAnimator.SetBool("Gun", true);
                        enemyAnimator.SetBool("Walk", false);
                        moveSpeed = 0;
                    }
                    else if (Player.transform.position.x >= transform.position.x)
                    {
                        
                        Shoot();
                        transform.localScale = new Vector3(-1, 1, 1);
                        enemyAnimator.SetBool("Gun", true);
                        enemyAnimator.SetBool("Walk", false);

                        moveSpeed = 0;


                    }
                     
                }
            }
        }
        if (!foundPlayer)
        {
            enemyAnimator.SetBool("Gun", false);
            enemyAnimator.SetBool("Walk", true);
            moveSpeed = 0.5f;

        }

    }


    private void Die()
    {
        if (SceneManager.GetActiveScene().name == "Level_1")
        {
            if (healthSystem.getHealth() <= 0)
            {
                Debug.Log("Creating PS");
                Instantiate(EnemyDie, transform.position, transform.rotation);
                Destroy(this.gameObject);
                Player.GetComponent<Player>().killCountL1 += 1;
            }
        }
        if (SceneManager.GetActiveScene().name == "Level_2")
        {
            if (healthSystem.getHealth() <= 0)
            {
                Debug.Log("Creating PS");
                Instantiate(EnemyDie, transform.position, transform.rotation);
                Destroy(this.gameObject);
                Player.GetComponent<Player>().killCountL2 += 1;
            }
        }

    }


    //Shoots based on Enemy location that it is facing.
    void Shoot()
    {
        if (!IsFacingRight() && delayShoot)
        {

            clone = Instantiate(Laser, sightEnd.position, Quaternion.identity);

            clone.GetComponent<bulletScript>().speed = 0.08f;

            StartCoroutine(noShoot());
        }
        else if (IsFacingRight() && delayShoot)
        {

            clone = Instantiate(Laser, sightEnd.position, Quaternion.identity);

            clone.GetComponent<bulletScript>().speed = -0.08f;
            StartCoroutine(noShoot());
        }
    }


    //Determines if enemy is facing right or left.
    bool IsFacingRight()
    {
        return transform.localScale.x > 0;
    }

    private IEnumerator noShoot()
    {
        delayShoot = false;
        yield return new WaitForSeconds(timebwnShot);
        delayShoot = true;
    }


    //Allows enemy to turn on Boundary is Collider trigger is hit.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == ("Boundary"))
        {

            if (IsFacingRight())
            {
                myRigidBody.velocity = new Vector2(moveSpeed, 0f);

            }
            else
            {
                myRigidBody.velocity = new Vector2(-moveSpeed, 0f);
            }
            transform.localScale = new Vector2(-(Mathf.Sign(myRigidBody.velocity.x)), 1f);
        }
        if (collision.gameObject.name == "AttackHitBox")
        {
            Debug.Log("Hitting enemy");
            Player.GetComponent<Player>().healthSystem.Damage(10);
        }
       
    }
}
