﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelControl : MonoBehaviour
{
    public static bool loaded;
    public Player player;
    private void Update()
    {
        loaded = Player.loaded;
        Scene ActiveScene = SceneManager.GetActiveScene();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Scene Apartment = SceneManager.GetSceneByName("Apartment");
            Scene Level1 = SceneManager.GetSceneByName("Level1");
            Scene Level2 = SceneManager.GetSceneByName("Level2");
            Scene ActiveScene = SceneManager.GetActiveScene();

                if (ActiveScene.name == "Apartment")
                {
                    SceneManager.LoadScene(3);
                }
                if (ActiveScene.name == "Level_1")
                {
                    SceneManager.LoadScene(4);
                }
                if (ActiveScene.name == "Level_2")
                {
                    player.transform.position = new Vector2(136.86f, -49.43f);
                }
                loaded = false;
        }
            
    }
    
}

