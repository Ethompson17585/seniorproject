﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointScript : MonoBehaviour
{
    private CheckpointMaster cm;
    public Player player;
    private void Start()
    {
        cm = GameObject.FindGameObjectWithTag("CM").GetComponent<CheckpointMaster>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            //Debug.Log("First checkpoint reached");
            cm.lastCheckPointposX = transform.position.x;
            cm.lastCheckPointposY = transform.position.y;
            //player.SavePlayer();
        }
    }
}
