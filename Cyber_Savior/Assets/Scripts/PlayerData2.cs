﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PlayerData2
{
    public int level;
    public int health;
    public float[] postition;

    public PlayerData2(Player player)
    {
        //level = player.sceneNumber;
        //health = player.playersHealth;

        postition = new float[2];
        postition[0] = player.transform.position.x;
        postition[1] = player.transform.position.y;
    }
}
