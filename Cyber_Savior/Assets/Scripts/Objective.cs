﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective : MonoBehaviour
{
    [SerializeField] GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(player.transform.position.x < 12)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: See Who Is Outside";
        }
        else if (player.transform.position.x < 20 && player.transform.position.x > 12)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: Meet Dave On Top Of Billboards";
        }
        else if (player.transform.position.x < 45 && player.transform.position.x > 20)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: Clear The Building";
        }
        else if (player.transform.position.x < 65 && player.transform.position.x > 45)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: Defeat The Robot";
        }
        else if (player.transform.position.x < 90 && player.transform.position.x > 70)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: Talk To Dave";
        }
        else if (player.transform.position.x < 109 && player.transform.position.x > 90)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: Clear The House";
        }
        else if (player.transform.position.x < 119 && player.transform.position.x > 109)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: Defeat the Eye Robot";
        }
        else if (player.transform.position.x < 123 && player.transform.position.x > 119)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: ";
        }
        else if (player.transform.position.x < 200 && player.transform.position.x > 140)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: Clear the Blue District";
        }
        else if (player.transform.position.x < 235 && player.transform.position.x > 200)
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Current Objective: Defeat Shade";
        }

    }
}
