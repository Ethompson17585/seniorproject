﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    Rigidbody2D rb;
    float delay = 3f;
    public int bulletVelocity;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Invoke("DestroySelf", .5f);
    }
    void FixedUpdate()
    {
        //Debug.Log(rb.velocity);
        rb.velocity = transform.right * bulletVelocity;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Foreground"))
        {
            destroyNow();
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<enemyLogic>().healthSystem.Damage(20);
            destroyNow();
        }
    }
    private void DestroySelf()
    {
        Destroy(gameObject, delay);
    }
    public void destroyNow()
    {
        Destroy(gameObject);
        Player.allowedToShoot = true;
    }
}
