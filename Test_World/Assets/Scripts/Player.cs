﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{
    [SerializeField] float runSpeed = 5f;
    [SerializeField] float jumpSpeed = 5f;
    [SerializeField] float climbSpeed = 5f;
    [SerializeField] Vector2 deathKick = new Vector2(25f, 25f);
    [SerializeField] GameObject Teleporter1;
    [SerializeField] GameObject Teleporter2;
    [SerializeField] GameObject Teleporter3;
    [SerializeField] GameObject Teleporter4;
    [SerializeField] GameObject HintText;
    [SerializeField] ParticleSystem ps;
    [SerializeField] int playersHealth = 3;
    [SerializeField] int damageTaken = 1;
    [SerializeField] float timeBtwDamage = 2f;
    bool isAlive = true;
    bool maskActive = false;
    public bool damageToPlayer = true;
    Animator myAnimator;
    Rigidbody2D myRigidBody;
    CapsuleCollider2D myBodyCollider;
    BoxCollider2D myFeet;
    float gravityScaleAtStart;
    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myBodyCollider = GetComponent<CapsuleCollider2D>();
        myFeet = GetComponent<BoxCollider2D>();
        gravityScaleAtStart = myRigidBody.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAlive) { return; }
        Run();
        Jump();
        enableMask();
        FlipSprite();
        ClimbLadder();
        Die();
        Teleport();
        Hint();

        //Debug.Log(playersHealth);
        
    }

    private void enableMask()
    {

        if (Input.GetKeyDown(KeyCode.Q))
        {
            maskActive = !maskActive;
            //if (maskActive == true)
            //{
            //    //myAnimator.SetBool("MaskDown", true);
            //}
            //else
            //{
            //    //myAnimator.SetBool("MaskUp", true);
            //}

        }
    }
    private void Run()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        float controlThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, myRigidBody.velocity.y);
        myRigidBody.velocity = playerVelocity;
        if (maskActive == true)
        {
            if (myRigidBody.velocity.x > 0 || myRigidBody.velocity.x < -0.1 && !Input.GetKey(KeyCode.LeftShift))
            {
                myAnimator.SetBool("IdleMask", false);
                myAnimator.SetBool("WalkMask", true);
                myAnimator.SetBool("Running", false);
            }
            else
            {
                myAnimator.SetBool("WalkMask", false);
                myAnimator.SetBool("IdleMask", true);
                myAnimator.SetBool("Running", false);
            }
        }
        if (maskActive == false)
        {
            if (myRigidBody.velocity.x > 0 || myRigidBody.velocity.x < -0.1) 
            {
                myAnimator.SetBool("Walk", true);
                myAnimator.SetBool("Idle", false);
            }
            else
            {
                myAnimator.SetBool("Walk", false);
                myAnimator.SetBool("Idle", true);
            }
                
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            runSpeed = 4.5f;
            maskActive = true;
            myAnimator.SetBool("Walk", false);
            myAnimator.SetBool("Running", true);
        }
        else 
        {
            runSpeed = 3.5f;
        }
    }
    private void ClimbLadder()
    {
        bool playerHasVerticalSpeed = Mathf.Abs(myRigidBody.velocity.y) > Mathf.Epsilon;
        if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Climbing")) && playerHasVerticalSpeed == false)
        {
            myAnimator.SetBool("StillClimb", true);
            myAnimator.SetBool("Climb", false);
        }
        if (!myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Climbing")))
        {
            if (maskActive)
            {
                myAnimator.SetBool("Climb", false);
                //myAnimator.SetBool("IdleMask", true);
                myRigidBody.gravityScale = gravityScaleAtStart;
                return;
            }
            else
            {
                myAnimator.SetBool("Climb", false);
                myAnimator.SetBool("Idle", true);
                myRigidBody.gravityScale = gravityScaleAtStart;
                return;
            }
        }
        if (!CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            float controlThrow = CrossPlatformInputManager.GetAxis("Vertical");
            Vector2 climbVelocity = new Vector2(myRigidBody.velocity.x, controlThrow * climbSpeed);
            myRigidBody.velocity = climbVelocity;
            myRigidBody.gravityScale = 0f;
            myAnimator.SetBool("StillClimb", false);
            myAnimator.SetBool("Climb", playerHasVerticalSpeed);
        }
       
        Debug.Log("player v speed" + playerHasVerticalSpeed);
        Debug.Log("player touching" + myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Climbing")));
    }
    private void Jump()
    {
        if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) && !myFeet.IsTouchingLayers(LayerMask.GetMask("Climbing")))
        {
            return;  
        }
        else if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            Vector2 jumpVelocityToAdd = new Vector2(0f, jumpSpeed);
            myRigidBody.velocity += jumpVelocityToAdd;
        }
    }
    public void Die()
    {

        if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Enemy", "Hazards", "Bullet")) && damageToPlayer)
        {
            playersHealth -= damageTaken;
            StartCoroutine(damageTimer());
            if (playersHealth <=0)
            {
                isAlive = false;
                myAnimator.SetTrigger("Die");
                GetComponent<Rigidbody2D>().velocity = deathKick;
            }
        }

        if (myRigidBody.velocity.y <= -16)
        {
            transform.position = new Vector2(-2, -3.4f);
            myRigidBody.velocity = Vector2.zero;
        }
    }
    private void FlipSprite()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        if(playerHasHorizontalSpeed) 
        {
            transform.localScale = new Vector2(Mathf.Sign(myRigidBody.velocity.x), 1f);
        }
    }

    private void Teleport()
    {
        if(myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Teleporter1")) && Input.GetKeyDown(KeyCode.E))
        {
            transform.position = new Vector2(Teleporter2.transform.position.x + 1, Teleporter2.transform.position.y);
            ps.Play();
        }
        else if(myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Teleporter2")) && Input.GetKeyDown(KeyCode.E))
        {
            transform.position = new Vector2(Teleporter1.transform.position.x - 1, Teleporter1.transform.position.y);
            ps.Play();
        }
        else if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Teleporter3")) && Input.GetKeyDown(KeyCode.E))
        {
            transform.position = new Vector2(Teleporter4.transform.position.x + 1, Teleporter4.transform.position.y);
            ps.Play();
        }
        else if (myBodyCollider.IsTouchingLayers(LayerMask.GetMask("Teleporter4")) && Input.GetKeyDown(KeyCode.E))
        {
            transform.position = new Vector2(Teleporter3.transform.position.x - 1, Teleporter3.transform.position.y);
            ps.Play();
        }
        else
        {
            return;
        }
    }

    private void Hint()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            HintText.SetActive(true);
        }
        if(Input.GetKeyUp(KeyCode.H))
        {
            HintText.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Bullet")
        { ///Debug.Log("hit");
            playersHealth -= damageTaken;
            
            if (playersHealth <=0)
            {
                isAlive = false;
                myAnimator.SetTrigger("Die");
                GetComponent<Rigidbody2D>().velocity = deathKick;
            }
        }

    }

    private IEnumerator damageTimer()
    {
        damageToPlayer = false;
        yield return new WaitForSeconds(timeBtwDamage);
        damageToPlayer = true;
    }
}
