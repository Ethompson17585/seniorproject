﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PlayerData
{
    public int level;
    public int health;
    public float[] postition;

    public PlayerData(Player player)
    {
        level = Player.sceneNumber;
        health = player.playersHealth;

        postition = new float[2];
        //postition[0] = cm.lastCheckPointposX;
        //postition[1] = cm.lastCheckPointposY;
    }
}
