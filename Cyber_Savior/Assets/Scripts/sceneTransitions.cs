﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneTransitions : MonoBehaviour
{
    public Animator transitionAnim;
    public string sceneName;


    public void transition()
    {
        transitionAnim.Play("EndTransition");
    }
    public void StartTransition()
    {
        SceneManager.LoadScene(sceneName);
        StartCoroutine(LoadScene());
    }
    IEnumerator LoadScene()
    {
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(sceneName);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
