﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletScript : MonoBehaviour
{
    public float speed = 0.08f;
    public GameObject Laser;
    public GameObject clone;
    

    // Update is called once per frame
    void Update()
    {
        Vector2 position = transform.position;
        position.x += speed;
        transform.position = position;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}