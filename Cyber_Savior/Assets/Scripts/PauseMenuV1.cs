﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PauseMenuV1: MonoBehaviour
{
    public Player player; 
	[SerializeField] private GameObject pauseMenuUI;
	
	[SerializeField] public static bool isPaused;
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
            if (isPaused)
		    {
			    Resume();
		    }
		    else
		    {
			    Pause();
		    }
		}
		
	}
	public void Pause()
	{
        pauseMenuUI.SetActive(true);
        //Time.timeScale = 0f;
        isPaused = true;
        player.paused = isPaused;
        //AudioListener.pause() = true;

    }
    public void Resume()
	{
        pauseMenuUI.SetActive(false);
		//Time.timeScale = 1f;
        isPaused = false;
        player.paused = isPaused;
        //AudioListener.pause() = false;

    }
    public void QuitGame()
    {
         SceneManager.LoadScene("MainMenu");
    }
    public void SaveGame()
    {

    }
}
